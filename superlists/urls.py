from django.conf.urls import include, url
from lists import views as list_views
from lists import urls as list_urls
# from lists.api import urls as api_urls
from lists.api import router

urlpatterns = [
    url(r'^$', list_views.home_page, name='home'),
    url(r'^lists/', include(list_urls)),
    # url(r'^api/', include(api_urls)),
    url(r'^api/', include(router.urls)),
]

